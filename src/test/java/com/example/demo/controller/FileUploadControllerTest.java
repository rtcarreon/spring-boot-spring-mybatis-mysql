package com.example.demo.controller;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.File;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
class FileUploadControllerTest extends BaseControllerTest {

	@Test
	void singleFileUploadTest() throws Exception {
		String response = "{\"message\":\"accepted\",\"responseCode\":202}";
	    
		// test file check, delete if exists
	    File file = new File(FileUploadController.UPLOADED_FOLDER + "test.txt");
	    file.delete();

	    MockMultipartFile mockMultipartFile = new MockMultipartFile(
	    		"demo-file",
	    		"test.txt",
	            "text/plain",
	            "test data".getBytes());
	    //MockMultipartFile jsonFile = new MockMultipartFile("json", "", "application/json", "{\"json\": \"someValue\"}".getBytes());
	    
	    MockHttpServletRequestBuilder builder =
	              MockMvcRequestBuilders.multipart("/upload/file")
	              		.file(mockMultipartFile)
	              		.header("Content-Type" , "application/json")
	        			.header("Demo-Token" , "j32io33ise4k2qq1");
	    
		MvcResult result = getMockMvc().perform(builder)
			 .andDo(MockMvcResultHandlers.print())
			 .andExpect(status().isCreated())
			 .andExpect(content().json(response))
             .andExpect(jsonPath("$.message", is("accepted")))
             .andExpect(jsonPath("$.responseCode", is(HttpStatus.ACCEPTED.value())))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(201, result.getResponse().getStatus());
    	File uploaded = new File(FileUploadController.UPLOADED_FOLDER + "test.txt");
    	assertTrue(uploaded.exists());
	}

	@Test
	void multiFileUploadTest() throws Exception {
		String response = "{\"message\":\"accepted\",\"responseCode\":202}";
	    
		// test file check, delete if exists
	    File file = new File(FileUploadController.UPLOADED_FOLDER + "test1.txt");
	    file.delete();
	    file = new File(FileUploadController.UPLOADED_FOLDER + "test2.txt");
	    file.delete();

	    MockMultipartFile multipartFile1 = new MockMultipartFile(
	    		"demo-file",
	    		"test1.txt",
	            "text/plain",
	            "test 1 data".getBytes());
	    
	    MockMultipartFile multipartFile2 = new MockMultipartFile(
	    		"demo-file",
	    		"test2.txt",
	            "text/plain",
	            "test 2 data".getBytes());
	    
	    MockHttpServletRequestBuilder builder =
	              MockMvcRequestBuilders.multipart("/upload/files")
	              		.file(multipartFile1)
	              		.file(multipartFile2)
	              		.header("Content-Type" , "application/json")
	        			.header("Demo-Token" , "j32io33ise4k2qq1");
	    
		MvcResult result = getMockMvc().perform(builder)
			 .andDo(MockMvcResultHandlers.print())
			 .andExpect(status().isCreated())
			 .andExpect(content().json(response))
             .andExpect(jsonPath("$.message", is("accepted")))
             .andExpect(jsonPath("$.responseCode", is(HttpStatus.ACCEPTED.value())))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(201, result.getResponse().getStatus());
    	
    	// check uploaded files
    	File uploaded1 = new File(FileUploadController.UPLOADED_FOLDER + "test1.txt");
    	File uploaded2 = new File(FileUploadController.UPLOADED_FOLDER + "test2.txt");
    	assertTrue(uploaded1.exists());
    	assertTrue(uploaded2.exists());
	}
}
