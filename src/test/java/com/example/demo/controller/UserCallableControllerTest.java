package com.example.demo.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.demo.controller.async.UserCallableController;
import com.example.demo.model.DemoResponse;
import com.example.demo.model.UserDetail;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class UserCallableControllerTest extends BaseControllerTest {	
	
    @MockBean
	UserService userService;
	
	@InjectMocks
    private UserCallableController userCallableController;
	
	@Test
	public void responseBodyCreateUserTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/response-body/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("success", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.CREATED.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("success")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
    	
	}
	
	@Test
	public void responseBodyCreateUserErrorTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(0L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/response-body/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	@Test
	public void responseBodyCreateUserThrowExceptionTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenThrow(RuntimeException.class);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/response-body/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	@Test
	public void customTimeoutCreateUserTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("success", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.CREATED.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("success")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
	}
	
	@Test
	public void customTimeoutCreateUserErrorTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(0L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	@Test
	public void customTimeoutCreateUserThrowExceptionTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenThrow(RuntimeException.class);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	@Test
	public void callableCustomTimeoutCreateUserTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/callable-custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("success", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.CREATED.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("success")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
	}
	
	@Test
	public void callableCustomTimeoutCreateUserErrorTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(0L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	@Test
	public void callableCustomTimeoutCreateUserThrowExceptionTest() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"bad request\",\"responseCode\":400}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenThrow(RuntimeException.class);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/user/async/custom-timeout/create")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("bad request", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.BAD_REQUEST.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("bad request")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.BAD_REQUEST.value())));    	
	}
	
	

	
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
}
