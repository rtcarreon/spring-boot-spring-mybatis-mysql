package com.example.demo.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.example.demo.controller.UserController;
import com.example.demo.model.DemoResponse;
import com.example.demo.model.UserDetail;
import com.example.demo.service.UserService;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest extends BaseControllerTest{	
	
    @MockBean
	UserService userService;
	
	@InjectMocks
    private UserController userController;
	
	@Test
	public void createUser() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	ResultActions result = getMockMvc().perform(MockMvcRequestBuilders.post("/createUser")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isCreated())
             .andExpect(content().json(response))
             .andExpect(jsonPath("$.message", is("success")))
             .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
    	
    	assertNotNull(result.andReturn().getResponse());
    	assertEquals( 
    			response, 
    			result.andReturn().getResponse().getContentAsString(), 
    			"Test json response");
	}
	
	@Test
	public void asyncCreateUser() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/async/createUser")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(DemoResponse.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(DemoResponse.class, result.getAsyncResult().getClass());
    	assertEquals("success", ((DemoResponse)result.getAsyncResult()).getMessage());
    	assertEquals(HttpStatus.CREATED.value(), ((DemoResponse)result.getAsyncResult()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("success")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
    	
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void async2CreateUser() throws Exception {
    	String content = "{\n" + 
    			"   \"username\": \"janeDoe\",\n" + 
    			"   \"fullName\": \"Jane Doe\",\n" + 
    			"   \"birthday\": \"02-05-1990\",\n" + 
    			"   \"gender\": \"female\",\n" + 
    			"   \"dateRegistered\": \"20-08-2012\"\n" + 
    			"}";
    	
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	Mockito.when(userService.createUser(Mockito.any(UserDetail.class))).thenReturn(1L);

    	MvcResult result = getMockMvc().perform(MockMvcRequestBuilders.post("/async2/createUser")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isOk())
    		 .andExpect(request().asyncStarted())
    		 .andExpect(request().asyncResult(instanceOf(ResponseEntity.class)))
    		 .andReturn();
    	assertNotNull(result.getResponse());
    	assertEquals(200, result.getResponse().getStatus());
    	
    	// assert async response
    	assertEquals(ResponseEntity.class, result.getAsyncResult().getClass());
    	assertEquals(DemoResponse.class, ((ResponseEntity)result.getAsyncResult()).getBody().getClass());
    	assertEquals("success", ((DemoResponse)((ResponseEntity)result.getAsyncResult()).getBody()).getMessage());
    	assertEquals(HttpStatus.CREATED.value(), ((DemoResponse)((ResponseEntity)result.getAsyncResult()).getBody()).getResponseCode());
    	
    	getMockMvc().perform(asyncDispatch(result))
			.andExpect(status().isCreated())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json(response))
            .andExpect(jsonPath("$.message", is("success")))
            .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void createUsers() throws Exception {
		String content = "{\n" + 
				"\"users\":\n" +
				"    [\n" + 
				"	    {\n" + 
    			"   	    \"username\": \"john123\",\n" + 
    			"   	    \"fullName\": \"John White Smith\",\n" + 
    			"   	    \"birthday\": \"02-05-1990\",\n" + 
    			"   	    \"gender\": \"male\",\n" + 
    			"   	    \"dateRegistered\": \"20-08-2012\"\n" + 
    			"       }, {\n" + 
    			"   	    \"username\": \"jackpeters\",\n" + 
    			"   	    \"fullName\": \"Jack Peters\",\n" + 
    			"   	    \"birthday\": \"11-06-1985\",\n" + 
    			"   	    \"gender\": \"male\",\n" + 
    			"   	    \"dateRegistered\": \"25-09-2014\"\n" + 
    			"       }\n" + 
    			"    ]\n" +
    			"}";
		
    	String response = "{\"message\":\"success\",\"responseCode\":201}";
    	
    	List<Long> ids = new ArrayList<>();
    	ids.add(1L);
    	ids.add(2L);
		Mockito.when(((List<Long>) userService.createUsers(Mockito.any(List.class)))).thenReturn(ids);
		
    	ResultActions result = getMockMvc().perform(MockMvcRequestBuilders.post("/createUsers")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    			.content(content)
    		).andExpect(status().isCreated())
             .andExpect(content().json(response))
             .andExpect(jsonPath("$.message", is("success")))
             .andExpect(jsonPath("$.responseCode", is(HttpStatus.CREATED.value())));
    	
    	assertNotNull(result.andReturn().getResponse());
    	assertEquals( 
    			response, 
    			result.andReturn().getResponse().getContentAsString(), 
    			"Test json response");
	}
	
	@Test
	public void getUser() throws Exception {
	    UserDetail user = new UserDetail();
	    user.setAge(10);
	    user.setBirthday(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("02-05-1990 08:00"));
	    user.setDateRegistered(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("20-08-2012 08:00"));
	    user.setFullName("Jane Doe");
	    user.setGender("female");
	    user.setUserId(1L);
    	user.setUsername("janeDoe");
    	
    	Mockito.when(userService.getByUsername("janeDoe")).thenReturn(user);

    	ResultActions result = getMockMvc().perform(MockMvcRequestBuilders.get("/getUser" + "/janeDoe")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    		).andExpect(status().isOk())
             .andExpect(jsonPath("$.age", is(10)))
             .andExpect(jsonPath("$.birthday", is("02-05-1990")))
             .andExpect(jsonPath("$.dateRegistered", is("20-08-2012")))
             .andExpect(jsonPath("$.fullName", is("Jane Doe")))
             .andExpect(jsonPath("$.gender", is("female")))
             .andExpect(jsonPath("$.userId", is(1)))
             .andExpect(jsonPath("$.username", is("janeDoe")));
    	
    	assertNotNull(result.andReturn().getResponse());
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"age\":10"), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"birthday\":\"02-05-1990\""), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"dateRegistered\":\"20-08-2012\""), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"fullName\":\"Jane Doe\""), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"gender\":\"female\""), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"userId\":1"), 
    			"Test json response");
    	assertTrue(
    			result.andReturn().getResponse().getContentAsString().contains("\"username\":\"janeDoe\""), 
    			"Test json response");
	}
	
	@Test
	public void getUsers() throws Exception {
		List<UserDetail> users = new ArrayList<>();
	    UserDetail user1 = new UserDetail();
	    user1.setAge(30);
	    user1.setBirthday(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("02-05-1989 08:00"));
	    user1.setDateRegistered(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("20-08-2012 08:00"));
	    user1.setFullName("John Doe");
	    user1.setGender("male");
	    user1.setUserId(1L);
    	user1.setUsername("johnDoe");
    	users.add(user1);
    	
    	UserDetail user2 = new UserDetail();
	    user2.setAge(29);
	    user2.setBirthday(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("02-05-1990 08:00"));
	    user2.setDateRegistered(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse("20-08-2012 08:00"));
	    user2.setFullName("Jane Doe");
	    user2.setGender("female");
	    user2.setUserId(2L);
    	user2.setUsername("janeDoe");
    	users.add(user2);
    	
    	Mockito.when(userService.getUsers()).thenReturn(users);

    	ResultActions result = getMockMvc().perform(MockMvcRequestBuilders.get("/getUsers")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    		).andExpect(status().isOk())
             .andExpect(jsonPath("$[0].age", is(30)))
             .andExpect(jsonPath("$[0].birthday", is("02-05-1989")))
             .andExpect(jsonPath("$[0].dateRegistered", is("20-08-2012")))
             .andExpect(jsonPath("$[0].fullName", is("John Doe")))
             .andExpect(jsonPath("$[0].gender", is("male")))
             .andExpect(jsonPath("$[0].userId", is(1)))
             .andExpect(jsonPath("$[0].username", is("johnDoe")))
             .andExpect(jsonPath("$[1].age", is(29)))
             .andExpect(jsonPath("$[1].birthday", is("02-05-1990")))
             .andExpect(jsonPath("$[1].dateRegistered", is("20-08-2012")))
             .andExpect(jsonPath("$[1].fullName", is("Jane Doe")))
             .andExpect(jsonPath("$[1].gender", is("female")))
             .andExpect(jsonPath("$[1].userId", is(2)))
             .andExpect(jsonPath("$[1].username", is("janeDoe")));
    	
    	assertNotNull(result.andReturn().getResponse());
	}
	
	@Test
	public void delete() throws Exception {
    	String response = "2";
    	
    	Mockito.when(userService.delete(1L)).thenReturn(2);

    	ResultActions result = getMockMvc().perform(MockMvcRequestBuilders.delete("/delete/1")
    			.accept(MediaType.APPLICATION_JSON)
    			.contentType(MediaType.APPLICATION_JSON)
    			.header("Content-Type" , "application/json")
    			.header("Demo-Token" , "j32io33ise4k2qq1")
    		).andExpect(status().isOk())
             .andExpect(content().json(response));
    	
    	assertNotNull(result.andReturn().getResponse());
    	assertEquals( 
    			response, 
    			result.andReturn().getResponse().getContentAsString(), 
    			"2");
	}
	
}
