package com.example.demo.controller;

import org.junit.After;
import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class BaseControllerTest {
	
	@Autowired
    private MockMvc mockMvc;
	
    @Autowired
    private WebApplicationContext wac;

	@Before
    public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        MockitoAnnotations.initMocks(this);
	}
	
	@After
    public void teardown() {
		
	}
	
	protected MockMvc getMockMvc() {
        return mockMvc;
    }
}
