package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.AccountMapper;
import com.example.demo.dao.UserMapper;
import com.example.demo.model.UserDetail;

@Transactional
@Service
public class UserService {
	
	@Autowired
    private UserMapper userMapper;
	
	@Autowired
    private AccountMapper accountMapper;
	
	public UserDetail getByUsername(String username) {
		UserDetail user = null;
		try {
			user = userMapper.getByUsername(username);
		} catch(Exception e) {
			e.printStackTrace();
			user = null;
		}
        return user;
    }
	
	public List<UserDetail> getUsers() {
		List<UserDetail> users = null;
		try {
			users = userMapper.getUsers();
		} catch(Exception e) {
			e.printStackTrace();
			users = null;
		}
        return users;
    }
	
	//@Transactional(propagation = Propagation.REQUIRES_NEW, 
	//		transactionManager = "transactionManager", 
	//		rollbackFor = {RuntimeException.class, SQLIntegrityConstraintViolationException.class, DuplicateKeyException.class, Exception.class})
	public long createUser(UserDetail user) throws Exception{
		long id = 0;
		try {
			int affected = userMapper.insert(user);
			affected += accountMapper.insert(user);
			if (affected != 2) {
				throw new Exception("number of inserted record is not equal to 2");
			}
			id = user.getUserId();
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return id;
	}
	
	@Transactional
	public List<Long> createUsers(List<UserDetail> users) throws Exception {
		List<Long> ids = new ArrayList<>();
		try {
			for (UserDetail user : users) {
				int affected = userMapper.insert(user);
				affected += accountMapper.insert(user);
				if (affected != 2) {
					throw new Exception("number of inserted record is not equal to 2");
				}
				
				ids.add(user.getUserId());
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return ids;
	}

	@Transactional
	public int updateUser(UserDetail user) {
		int affected = 0;
		try {
			affected = userMapper.update(user);
			if (affected == 0) {
				throw new Exception("affected record is 0");
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return affected;
	}

	@Transactional
	public int updateUsers(List<UserDetail> users) {
		int affected = 0;
		try {
			for (UserDetail user : users) {
				affected += userMapper.update(user);
			}
			
			if (affected == 0 || affected != users.size()) {
				throw new Exception("affected record is 0 or affected records not equal to number of request");
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return affected;
	}
	
	@Transactional
	public int activate(long userId) {
		int affected = 0;
		try {
			affected = accountMapper.activate(userId);
			if (affected == 0) {
				throw new Exception("no activated record");
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return affected;
	}
	
	@Transactional
	public int deactivate(long userId) {
		int affected = 0;
		try {
			affected = accountMapper.deactivate(userId);
			if (affected == 0) {
				throw new Exception("no deactivated record");
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return affected;
	}
	
	@Transactional
	public int delete(long userId) {
		int affected = 0;
		try {
			affected += accountMapper.delete(userId);
			affected += userMapper.delete(userId);
			if (affected == 0) {
				throw new Exception("no deleted record/s");
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		return affected;
	}
}
