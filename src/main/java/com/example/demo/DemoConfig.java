package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ComponentScan(basePackages = {
		"com.example.demo",
		"com.example.demo.controller",
		"com.example.demo.controller.async",
		"com.example.demo.dao",
		"com.example.demo.service"
})
@Configuration
@PropertySources(value = {
		@PropertySource("classpath:application.properties"),
		@PropertySource("classpath:appConfig.properties")
})
@EnableTransactionManagement
public class DemoConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
