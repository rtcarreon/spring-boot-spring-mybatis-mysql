package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.DemoResponse;
import com.example.demo.model.FormDataWithFile;

@Controller
@RequestMapping("/upload")
public class FileUploadController extends BaseController {

	
	//Save the uploaded file to this folder
    protected static String UPLOADED_FOLDER = "/tmp//";
	
    @RequestMapping(value="/file",
    		method = RequestMethod.POST
    )
    public ResponseEntity<DemoResponse> singleFileUpload(
    		@RequestHeader("Demo-Token") String demoToken,
    		@RequestParam("demo-file") MultipartFile file) {
    	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    	
    	if (!file.isEmpty()) {
            logDebug("File OriginalFilename", file.getOriginalFilename());
            logDebug("File Name", file.getName());
            logDebug("File Content Type", file.getContentType());
            logDebug("File Size", "" + file.getSize());
            
            try {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                Files.write(path, bytes);

                
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
        	response = ResponseEntity.badRequest()
        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
        	logDebug("", "file is empty");
        }
    	
    	return response;
    }
    
    @RequestMapping(value="/files",
    		method = RequestMethod.POST
    )
    public ResponseEntity<DemoResponse> multiFileUpload(
    		@RequestHeader("Demo-Token") String demoToken,
    		@RequestParam("demo-file") MultipartFile[] files) {
    	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    	
    	if (files != null && files.length > 0) {
    		logDebug("number of files", "" + files.length);
    		
    		for ( MultipartFile file : files) {
    			logDebug("File OriginalFilename", file.getOriginalFilename());
                logDebug("File Name", file.getName());
                logDebug("File Content Type", file.getContentType());
                logDebug("File Size", "" + file.getSize());
                
                try {
                    byte[] bytes = file.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                    Files.write(path, bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                    logError("", e.getMessage());
                }
    		}
    		
            return response;
        } else {
        	logDebug("no file", "no file/s");
        	response = ResponseEntity.badRequest()
        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
        }
    	
    	return response;
    }
    
    @RequestMapping(value="/form-data",
    		method = RequestMethod.POST
    )
    public ResponseEntity<DemoResponse> formDataUpload(
    		@RequestHeader("Demo-Token") String demoToken,
    		@ModelAttribute FormDataWithFile formData) {
    	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    	
    	logDebug("Form data name", formData.getName());
    	logDebug("Form data email", formData.getEmail());
    	
    	if (formData.getFile() != null && !formData.getFile().isEmpty()) {
    		logDebug("File OriginalFilename", formData.getFile().getOriginalFilename());
            logDebug("File Name", formData.getFile().getName());
            logDebug("File Content Type", formData.getFile().getContentType());
            logDebug("File Size", "" + formData.getFile().getSize());
            
            try {
                byte[] bytes = formData.getFile().getBytes();
                Path path = Paths.get(UPLOADED_FOLDER + formData.getFile().getOriginalFilename());
                Files.write(path, bytes);

                
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
        	response = ResponseEntity.badRequest()
        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
        	logDebug("", "file is empty");
        }
    	
    	return response;
    }
}
