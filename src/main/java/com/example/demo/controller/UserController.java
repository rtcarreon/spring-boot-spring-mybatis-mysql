package com.example.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;

import com.example.demo.model.DemoResponse;
import com.example.demo.model.UserDetail;
import com.example.demo.model.UserList;
import com.example.demo.service.UserService;

@RestController
public class UserController extends BaseController {
	
	@Autowired
	@Qualifier("userService")
	private UserService userService;
	
    @RequestMapping(value="/createUser",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DemoResponse> createUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {
    	
    	// validate header token
    	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    	
    	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		long id = 0;
    		try {
    			id = userService.createUser(user);
    		} catch(Exception e) {
    			log.error(e.getMessage());
    			log.error(e.getLocalizedMessage());
    		}
	    	
	    	// user not created
	    	if (id == 0) {
	    		response = ResponseEntity.badRequest()
	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	    		
	    	// user is created
	    	} else {
	    		response = ResponseEntity
	        			.created(null)
	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
	        }
    	}
    	
    	return response;
    }
    

    @RequestMapping(value="/async/createUser",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Callable<DemoResponse> asyncCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {
    	
    	return () -> {
    		DemoResponse resp = null;
    		long id = 0;
    		
    		try {
    			id = userService.createUser(user);
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		
    		//TODO for testing purpose only, remove the code below
    		Thread.sleep(2000);
    		
    		// user not created
	    	if (id == 0) {
	    		resp = new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value());
	    		
	    	// user is created
	    	} else {
	    		resp = new DemoResponse("success", HttpStatus.CREATED.value());
	        }
            return resp;
        };
    }
    
    @RequestMapping(value="/async2/createUser",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebAsyncTask<ResponseEntity<DemoResponse>> async2CreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {
    	    	
    	return new WebAsyncTask<ResponseEntity<DemoResponse>> (6000, () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	
        	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
        		long id = 0;
        		try {
        			id = userService.createUser(user);
        		} catch(Exception e) {
        			log.error(e.getMessage());
        		}
        		
        		//TODO for testing purpose only, remove the code below
        		Thread.sleep(2000);
    	    	
    	    	// user not created
    	    	if (id == 0) {
    	    		response = ResponseEntity.badRequest()
    	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
    	    		
    	    	// user is created
    	    	} else {
    	    		response = ResponseEntity
    	        			.created(null)
    	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
    	        }
        	}
    		
            return response;
        }); 
    }

    @RequestMapping(value = "/createUsers",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DemoResponse> createUsers(
    		@RequestHeader("Demo-Token") String demoToken,
    		@RequestBody UserList users) {
    	
    	// validate header token
    	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    	
    	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		List<Long> ids = null;
    		
    		try {
    			ids = userService.createUsers(Arrays.asList(users.getUsers()));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		
    		// all user/s created
	    	if (ids != null && ids.size() == users.getUsers().length) {
	    		response = ResponseEntity.created(null)
	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
	    		
	    	// user/s not created
	    	} else {
	    		response = ResponseEntity.badRequest()
	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	        }
    	}

    	return response;
    }

    @SuppressWarnings({ "rawtypes" })
	@RequestMapping(value="/getUser/{username}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getByUsername (
    		@RequestHeader("Demo-Token") String demoToken, 
    		@PathVariable("username") String username) {
    	
    	log.debug("Debugging log");
        log.info("Info log");
        log.warn("Hey, This is a warning!");
        log.error("Oops! We have an Error. OK");
        log.fatal("Damn! Fatal error. Please fix me.");
        
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		UserDetail user = userService.getByUsername(username);
    		if (user != null ) {
    			response = new ResponseEntity<>(user, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("user not found", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    		}
    	}
    	
    	return response;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/getUsers",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUsers(
    		@RequestHeader("Demo-Token") String demoToken) {
    	
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		List<UserDetail> users = userService.getUsers();
    		if (users != null && !users.isEmpty()) {
    			response = new ResponseEntity<>(users, HttpStatus.OK);
    			
    		// if there is an issue with the database or there is actually no user/s
    		} else {
    			((ResponseEntity<DemoResponse>)response).getBody().setMessage("system error");
    			((ResponseEntity<DemoResponse>)response).getBody().setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    		}
    	}

    	return response;
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value="/updateUser",
    		method = RequestMethod.PATCH,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@RequestBody UserDetail user, @RequestHeader("Demo-Token") String demoToken) {
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		int affected = 0;
        	try {
        		affected = userService.updateUser(user);
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		if (affected != 0 ) {
    			response = new ResponseEntity<>(affected, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("not modified", HttpStatus.NOT_MODIFIED.value()), HttpStatus.NOT_MODIFIED);
    		}
    	}
    	
    	return response;
    }
    
    @SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/updateUsers",
    		method = RequestMethod.PATCH,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUsers(@RequestBody UserList users, @RequestHeader("Demo-Token") String demoToken) {
    	// validate header token
		ResponseEntity response = authenticate(demoToken);
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		int affected = 0;
        	try {
        		affected = userService.updateUsers(Arrays.asList(users.getUsers()));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		if (affected != 0 ) {
    			response = new ResponseEntity<>(users.getUsers().length, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("not modified", HttpStatus.NOT_MODIFIED.value()), HttpStatus.NOT_MODIFIED);
    		}
    	}
    	
    	return response;
    }
    
    @SuppressWarnings("rawtypes")
	@RequestMapping(value="/activate/{userId}",
    		method = RequestMethod.PATCH,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity activate(@PathVariable("userId") String userId, @RequestHeader("Demo-Token") String demoToken) {    	
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		int affected = 0;
        	try {
        		affected = userService.activate(Long.valueOf(userId));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		if (affected != 0 ) {
    			response = new ResponseEntity<>(affected, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("not modified", HttpStatus.NOT_MODIFIED.value()), HttpStatus.NOT_MODIFIED);
    		}
    	}
    	
    	return response;
    }
    
    @SuppressWarnings("rawtypes")
	@RequestMapping(value="/deactivate/{userId}",
    		method = RequestMethod.PATCH,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deactivate(@PathVariable("userId") String userId, @RequestHeader("Demo-Token") String demoToken) {    	
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		int affected = 0;
        	try {
        		affected = userService.deactivate(Long.valueOf(userId));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		if (affected != 0 ) {
    			response = new ResponseEntity<>(affected, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("not modified", HttpStatus.NOT_MODIFIED.value()), HttpStatus.NOT_MODIFIED);
    		}
    	}
    	
    	return response;
    }
    
    @SuppressWarnings("rawtypes")
	@RequestMapping(value="/delete/{userId}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(
    		@PathVariable("userId") String userId,
    		@RequestHeader("Demo-Token") String demoToken) {
    	
    	logDebug("userId", userId);
    	// validate header token
    	ResponseEntity response = authenticate(demoToken);
    	
    	if (((DemoResponse)response.getBody()).getResponseCode() == HttpStatus.ACCEPTED.value()) {
    		int affected = 0;
        	try {
        		affected = userService.delete(Long.valueOf(userId));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		if (affected > 0 ) {
    			response = new ResponseEntity<>(affected, HttpStatus.OK);
    		} else {    			
    			response = new ResponseEntity<>(new DemoResponse("conflict with the target source", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
    		}
    	}
    	
    	return response;
    }

}