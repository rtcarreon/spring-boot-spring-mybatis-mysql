package com.example.demo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.model.DemoResponse;

public abstract class BaseController {
	
	protected final Logger log = LogManager.getLogger(this.getClass());
	
	@Value("${demo.token}")
    private String appToken;

	
	private boolean validate(String token) {
		return token != null ? token.equals(appToken) : false;
	}
	
	protected ResponseEntity<DemoResponse> authenticate(String token) {
		ResponseEntity<DemoResponse> response = ResponseEntity.created(null)
    			.body(new DemoResponse("accepted", HttpStatus.ACCEPTED.value()));
    	
    	if (!validate(token)) {
    		response = ResponseEntity.badRequest()
        			.body(new DemoResponse("Unauthorized", HttpStatus.UNAUTHORIZED.value()));
    	}
    	
    	return response;
	}
	
	protected void logDebug(String message, String value) {
		log.debug(message + " :: " + value);
	}
		
	protected void logInfo(String message, String value) {
		log.info(message + " :: " + value);
	}
	
	protected void logError(String message, String value) {
		log.error(message + " :: " + value);
	}
	
	protected void logFatal(String message, String value) {
		log.fatal(message + " :: " + value);
	}
	
	protected void logTrace(String message, String value) {
		log.trace(message + " :: " + value);
	}
	
	protected void logWarn(String message, String value) {
		log.warn(message + " :: " + value);
	}
}
