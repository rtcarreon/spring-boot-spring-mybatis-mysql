package com.example.demo.controller.async;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;
import com.example.demo.controller.BaseController;
import com.example.demo.model.DemoResponse;
import com.example.demo.model.UserDetail;
import com.example.demo.model.UserList;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/user/async")
public class UserCallableController extends BaseController {
		
	@Autowired
	@Qualifier("userService")
	private UserService userService;
	
    @RequestMapping(value="/response-body/create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Callable<DemoResponse> responseBodyCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {
    	
    	logDebug("token", demoToken);
    	logDebug("Username", user.getUsername());
    	logDebug("Full name", user.getFullName());
    	logDebug("Gender", user.getGender());
    	logDebug("Birthday", user.getBirthday().toString());
    	logDebug("Date registered", user.getDateRegistered().toString());
    	
    	return () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	
    		long id = 0;
    		
    		try {
    			id = userService.createUser(user);
    		} catch(Exception e) {
    			logError("", e.getMessage());
    		}
    		
    		//TODO for testing purpose only, remove the code below
    		Thread.sleep(2000);
    		
    		if (id == 0) {
	    		response = ResponseEntity.badRequest()
	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	    		
	    	// user is created
	    	} else {
	    		response = ResponseEntity
	        			.created(null)
	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
	        }
	    	
            return response.getBody();
        };
    }
    
    @RequestMapping(value="/custom-timeout/create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebAsyncTask<DemoResponse> customTimeoutCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {
    	    	
    	logDebug("token", demoToken);
    	logDebug("Username", user.getUsername());
    	logDebug("Full name", user.getFullName());
    	logDebug("Gender", user.getGender());
    	logDebug("Birthday", user.getBirthday().toString());
    	logDebug("Date registered", user.getDateRegistered().toString());
    	    	
    	return new WebAsyncTask<DemoResponse> (6000, () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	
        	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
        		long id = 0;
        		try {
        			id = userService.createUser(user);
        		} catch(Exception e) {
        			logError("", e.getMessage());
        		}
        		
        		//TODO for testing purpose only, remove the code below
        		Thread.sleep(2000);
    	    	
    	    	// user not created
    	    	if (id == 0) {
    	    		response = ResponseEntity.badRequest()
    	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
    	    		
    	    	// user is created
    	    	} else {
    	    		response = ResponseEntity
    	        			.created(null)
    	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
    	        }
        	}
    		
            return response.getBody();
        }); 
    }
    
    @RequestMapping(value="/callable-custom-timeout/create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebAsyncTask<DemoResponse> callableCustomTimeoutCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserDetail user) {

    	logDebug("token", demoToken);
    	logDebug("Username", user.getUsername());
    	logDebug("Full name", user.getFullName());
    	logDebug("Gender", user.getGender());
    	logDebug("Birthday", user.getBirthday().toString());
    	logDebug("Date registered", user.getDateRegistered().toString());
    	
    	Callable<DemoResponse> callable = () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	
        	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
        		long id = 0;
        		try {
        			id = userService.createUser(user);
        		} catch(Exception e) {
        			logError("", e.getMessage());
        		}
        		
        		//TODO for testing purpose only, remove the code below
        		Thread.sleep(2000);
    	    	
    	    	// user not created
    	    	if (id == 0) {
    	    		response = ResponseEntity.badRequest()
    	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
    	    		
    	    	// user is created
    	    	} else {
    	    		response = ResponseEntity
    	        			.created(null)
    	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
    	        }
        	}
    		
            return response.getBody();
		};
		
		return new WebAsyncTask<DemoResponse>(3000, callable);
    }
    
    @RequestMapping(value="/response-body/multi-create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Callable<DemoResponse> responseBodyCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserList users) {
    	
    	logDebug("token", demoToken);
    	logDebug("username", users.toString());
    	
    	return () -> {

        	// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
    		List<Long> ids = null;
    		
    		try {
    			ids = userService.createUsers(Arrays.asList(users.getUsers()));
    		} catch(Exception e) {
    			log.error(e.getMessage());
    		}
    		
    		//TODO for testing purpose only, remove the code below
    		Thread.sleep(2000);
    		
    		// all user/s created
	    	if (ids != null && ids.size() == users.getUsers().length) {
	    		response = ResponseEntity.created(null)
	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
	    		
	    	// user/s not created
	    	} else {
	    		response = ResponseEntity.badRequest()
	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	        }
	    	
            return response.getBody();
        };
    }
    
    @RequestMapping(value="/custom-timeout/multi-create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebAsyncTask<DemoResponse> customTimeoutCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserList users) {
    	
    	logDebug("token", demoToken);
    	logDebug("username", users.toString());
    	
    	return new WebAsyncTask<DemoResponse> (6000, () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	    	
        	if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
        		List<Long> ids = null;
        		try {
        			ids = userService.createUsers(Arrays.asList(users.getUsers()));
        		} catch(Exception e) {
        			logError("create users", e.getMessage());
        		}
        		
        		// all user/s created
    	    	if (ids != null && ids.size() == users.getUsers().length) {
    	    		response = ResponseEntity.created(null)
    	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
    	    		
    	    	// user/s not created
    	    	} else {
    	    		response = ResponseEntity.badRequest()
    	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
    	        }
        	}
    		
            return response.getBody();
        }); 
    }
    
    @RequestMapping(value="/callable-custom-timeout/multi-create",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody WebAsyncTask<DemoResponse> callableCustomTimeoutCreateUser(
    		@RequestHeader("Demo-Token") String demoToken, 
    		@RequestBody UserList users) {
    	
    	logDebug("token", demoToken);
    	logDebug("username", users.toString());
    	
    	Callable<DemoResponse> callable = () -> {
    		// validate header token
        	ResponseEntity<DemoResponse> response = authenticate(demoToken);
        	
    		if (response.getBody().getResponseCode() == HttpStatus.ACCEPTED.value()) {
        		List<Long> ids = null;
        		
        		try {
        			ids = userService.createUsers(Arrays.asList(users.getUsers()));
        		} catch(Exception e) {
        			log.error(e.getMessage());
        		}
        		
        		// all user/s created
    	    	if (ids != null && ids.size() == users.getUsers().length) {
    	    		response = ResponseEntity.created(null)
    	        			.body(new DemoResponse("success", HttpStatus.CREATED.value()));
    	    		
    	    	// user/s not created
    	    	} else {
    	    		response = ResponseEntity.badRequest()
    	        			.body(new DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
    	        }
        	}
    		
            return response.getBody();
		};
		
		return new WebAsyncTask<DemoResponse>(3000, callable);
    }
}