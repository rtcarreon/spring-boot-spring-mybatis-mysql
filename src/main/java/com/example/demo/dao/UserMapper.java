package com.example.demo.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.UserDetail;

import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Result;;


@Mapper
public interface UserMapper {
	
	@Results(
	    id = "getByUsernameResult", 
	    value = {
	    	@Result(property = "userId", column = "id", id = true),
			@Result(property = "username", column = "username"),
			@Result(property = "fullName", column = "full_name"),
			@Result(property = "age", column = "age"),
			@Result(property = "gender", column = "gender"),
			@Result(property = "birthday", column = "birthday", jdbcType = JdbcType.DATE, javaType = Date.class),
			@Result(property = "dateRegistered", column = "date_registered", jdbcType = JdbcType.DATE, javaType = Date.class)
		})
	@Select(
		"SELECT ud.id AS id, "
		+ "    a.username AS username, "
		+ "    ud.full_name AS full_name, "
		+ "    CASE ud.gender "
		+ "        WHEN 'M' THEN 'male' "
		+ "        WHEN 'F' THEN 'female' "
		+ "    END AS gender, "
		+ "    TIMESTAMPDIFF(YEAR, ud.birthday, CURDATE()) AS age, "
		+ "    ud.birthday AS birthday, "
		+ "    a.date_registered AS date_registered "
		+ " FROM  Account a "
		+ " INNER JOIN UserDetails ud ON  ud.id = a.user_id "
		+ " WHERE a.active = 'ACTIVE' AND a.username = #{username}; ")
	UserDetail getByUsername(String username) throws Exception;
	
	@Results(
		id = "getUsersResult", 
		value = {
			@Result(property = "userId", column = "id", id = true),
			@Result(property = "username", column = "username"),
			@Result(property = "fullName", column = "full_name"),
			@Result(property = "age", column = "age"),
			@Result(property = "gender", column = "gender"),
			@Result(property = "birthday", column = "birthday", javaType = Date.class),
			@Result(property = "dateRegistered", column = "date_registered", javaType = Date.class)
		})
	@Select(
		"SELECT ud.id AS id, "
		+ "		a.username AS username, "
		+ "		ud.full_name AS full_name, "
		+ " 	CASE ud.gender "
		+ "        	WHEN 'M' THEN 'male' "
		+ "        	WHEN 'F' THEN 'female' "
		+ "    	END AS gender, "
		+ "		TIMESTAMPDIFF(YEAR, ud.birthday, CURDATE()) AS age, "
		+ "		ud.birthday AS birthday, "
		+ "		a.date_registered AS date_registered "
		+ " FROM  Account a "
		+ " INNER JOIN UserDetails ud ON  ud.id = a.user_id "
		+ " WHERE a.active = 'ACTIVE'; ")
	List<UserDetail> getUsers() throws Exception;
	
	@Insert(
		"INSERT INTO UserDetails "
        + "    (full_name, birthday, gender) "
        + " VALUES ("
        + "    #{fullName}, "
        + "	   #{birthday}, "
        + "    CASE" 
        + "        WHEN LOWER('${gender}') = 'male' "
        + "		       THEN 'M' "
        + "            ELSE 'F' "
        + "    END); ")
	//@SelectKey(statement="select STANDARDS_ID_SEQ.CURRVAL from dual", resultType = int.class, before = false, keyProperty = ID)
	@Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "id")
    int insert(UserDetail user) throws Exception;
    
	@Update(
		"UPDATE UserDetails "
		+ " SET"
        + "    full_name = #{fullName}, "
        + "    birthday = #{birthday}, "
        + "    gender = "
        + "	       CASE "
        + "            WHEN LOWER('${gender}') = 'male' "
        + "            THEN 'M' "
        + "            ELSE 'F' "
        + "         END "
        + " WHERE id = #{userId}; ")
    int update(UserDetail user) throws Exception;
    

	@Delete("DELETE FROM UserDetails WHERE id = #{userId}; ")
    int delete(long userId) throws Exception;
	
	
	
}
