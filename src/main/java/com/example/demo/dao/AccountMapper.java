package com.example.demo.dao;


import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import com.example.demo.model.UserDetail;

@Mapper
public interface AccountMapper {
	@Insert(
		"INSERT INTO Account "
		+ "    (user_id, username, date_registered) "
		+ " VALUES ( "
        + "    #{userId}, "
        + "    #{username}, "
        + "    #{dateRegistered} "
        + " ); ")
    int insert(UserDetail account) throws Exception;
    
	@Update(
		"UPDATE Account "
		+ " SET date_registered = #{dateRegistered} "
		+ " WHERE user_id = #{userId}; ")
    int update(UserDetail account) throws Exception;
	
	@Update(
		"UPDATE Account "
		+ " SET active = 'ACTIVE' "
		+ " WHERE user_id = #{userId}; ")
    int activate(long userId) throws Exception;
	
	@Update(
		"UPDATE Account "
		+ " SET active = 'INACTIVE' "
		+ " WHERE user_id = #{userId}; ")
    int deactivate(long userId) throws Exception;
    
	@Delete("DELETE FROM Account WHERE user_id = #{userId}; ")
    int delete(long userId) throws Exception;

}
