package com.example.demo.model;

public class UserList {
	private UserDetail[] users;

	public UserDetail[] getUsers() {
		return users;
	}

	public void setUsers(UserDetail[] users) {
		this.users = users;
	}
}
