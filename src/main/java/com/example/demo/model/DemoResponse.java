package com.example.demo.model;

public class DemoResponse {
	private String message;
	private int responseCode;
	
	public DemoResponse(String message, int responseCode) {
		this.message = message;
		this.responseCode = responseCode;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
}
